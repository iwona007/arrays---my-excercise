package Arrays;

public class    ArrayWieloForEach {
    public static void main(String[] args) {

        int[][] tab = new int[5][5];
        int licznikLiczb = 0;
        for (int i = 0; i < tab.length; i++) {
            for (int j = 0; j < tab[i].length; j++) {
                // ja mnoże tutaj przez kolumny, czyli i wiersze * wymiar kolumn
                tab[i][j] = i * 5 + j + 1;
              //  tab [i][j] = ++licznikLiczb;
                System.out.print(tab[i][j] + "\t");
            }
            System.out.println();
        }
        System.out.println("**************");
        for (int[] wiersz : tab) {
            for (int element : wiersz) {
                System.out.print(element + "\t");
            }
            System.out.println();
        }
        System.out.println("**************");


        int[][] tablicaDwuwymiarowa = {{1, 2, 3, 4}, {5, 6, 7}, {9, 10, 11, 12}, {13, 14, 15, 16}};

        for (int[] wiersz : tablicaDwuwymiarowa) {
            for (int kolumna : wiersz) {
                System.out.print(kolumna + "\t");
            }
            System.out.println();
        }
    }
//31 March 2019
}

