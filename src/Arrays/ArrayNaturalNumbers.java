package Arrays;

import java.util.StringJoiner;

public class ArrayNaturalNumbers {
    public static void main(String[] args) {

        // Zainicjalizuj tablicę naturalne z 10 wartościami i wydrukuj w formacie
// tablica:{1,2,3,4,5,6,7,8,9,10}

        int[] tablicaNaturalne = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
        System.out.println("tablica:{" + tablicaNaturalne[0]+"," + tablicaNaturalne[1]+ "," + tablicaNaturalne[2] + "}");

        int[] tabNaturalne2 = new int[10];
        tabNaturalne2[0] = 1;
        tabNaturalne2[1] = 2;
        tabNaturalne2[2] = 3;
        tabNaturalne2[3] = 4;
        tabNaturalne2[4] = 5;
        tabNaturalne2[5] = 6;
        tabNaturalne2[6] = 7;
        tabNaturalne2[7] = 8;
        tabNaturalne2[8] = 9;
        tabNaturalne2[9] = 10;

        System.out.println("Naturalne 2: {" + tabNaturalne2[0] + "," +tabNaturalne2[1] +"," + tabNaturalne2[2] +","
                + tabNaturalne2[3] + "," +tabNaturalne2[4] +"," + tabNaturalne2[5] +"," + tabNaturalne2[6] +","
                + tabNaturalne2[7] +"," + tabNaturalne2[8] +"," + tabNaturalne2[9] + "}");


        int tabNaturalne3[] = new int[10];
        for (int naturalne3 = 0; naturalne3 < tabNaturalne3.length; naturalne3++) {
            tabNaturalne3[naturalne3] = naturalne3 + 1;
            System.out.print("{" + tabNaturalne3[naturalne3] + "}");
        }
        System.out.println();

        System.out.print('\n' + "tablica naturalne: {");
        for (int naturalne = 0; naturalne < tablicaNaturalne.length; naturalne++) {
            System.out.print(tablicaNaturalne[naturalne] + (naturalne == tablicaNaturalne.length - 1 ? "" : ","));

        }
        System.out.print("}");
        System.out.println();
        System.out.println(tablicaNaturalne.length);


    }

}
