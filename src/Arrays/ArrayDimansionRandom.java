package Arrays;

import java.util.Random;

public class ArrayDimansionRandom {
    public static void main(String[] args) {

        int [][] array = new int [2][10];
        int min = 1;
        int max = 10;
        int range = max- min +1;

        for (int i = 0; i< array.length; i++){
            for(int j = 0; j< array[i].length; j++){
               array[i][j] = (int) (Math.random() * range)+min;
                System.out.print(array[i][j] + "\t");
            }
            System.out.println();
            System.out.println("*************");
            System.out.println();
        }

        //obiektowo

        Random losowo = new Random ();
        int [][] array2 = new int [3][10];
        int liczbyLosowe = 45;

        for(int a =0; a< array2.length; a++){
            for (int b =0; b< array2[a].length; b++){
                array2[a][b] += liczbyLosowe;
                System.out.print(losowo.nextInt(45)+ "\t");
            }
            System.out.println();
        }


    }
}
