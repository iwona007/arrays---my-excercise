package Arrays2;

public class Do99Wielowymiarowa {
    public static void main(String[] args) {

        int[][] tablica2D = new int[10][10];

        for (int i = 0; i < tablica2D.length; i++) {
            for (int j = 0; j < tablica2D[i].length; j++) {
                tablica2D[i][j] = i * tablica2D.length + j;
            }
        }
        for (int[] tablica1D : tablica2D) {
            for (int liczba : tablica1D) {
                if (liczba < 10) {
                    System.out.print("0" + liczba + ", ");
                } else {
                    System.out.print(liczba + ", ");
                }
            }
            System.out.println();
        }
    }
}

